###############################################
# returns an array of gene objects and all relationships to 
# dataset, species, homology, etc. tables #
##############################################
from models.gene import Gene
from models.model import db, count_by_query
from sqlalchemy import or_, and_, desc
import json
 
def genes_by_genename(query):
    orQuery = list()
    list_of_genes = list()

    list_of_genes = query.split(",")

    for each in list_of_genes:
        orQuery.append("genes.symbol = '" + each + "'")
    
    final_query = or_(*orQuery)
    geneSet = db.genes.filter(final_query).all()

    if len(geneSet) > 0:
        gene_list = []
        
        for each_gene in geneSet:
            one_gene_obj=Gene(each_gene.entrez_gene_id)
            gene_list.append(one_gene_obj)

    return gene_list

##db.genes.filter(db.genes.symbol == query).all()

def geneSlice(startNum, numRows): 
    return db.genes.order_by(db.genes.entrez_gene_id)[startNum:numRows]

def getGeneObjsSlice(start, numRows):
    finalGeneArray = list()
    maxNum = start + numRows
    geneObjs = geneSlice(start, maxNum)
    for gene in geneObjs:
        finalGeneArray.append(Gene(gene.entrez_gene_id))
    return finalGeneArray

def multiGeneSignals(geneObjList):
    masterList = {}
    geneList = list()
    buttonList = list()
    datasetList = list()
    datasetDispList = list()
    finalDataList = list()
 
    geneCount = 1

    for each in geneObjList:
 
        geneList.append(each.geneName)
        buttonList.append((each.entrezId))
        breadthDict = each.breadthByDsId()
        biosampleByDsIdDict = each.biosampleByDsId()
        
        ## go through each dataset->peak pair ##
        for key, value in breadthDict.iteritems():
            datasetList.append(str(key))
            datasetDispList.append(biosampleByDsIdDict[str(key)])

            if key in masterList:
                masterList[key].append(str(value))
            else:
                if geneCount == 1:
                    masterList[key] = list()
                    masterList[key].append(str(value))
                
                else:
                    masterList[key]= list("0")  #adding 0 for genes that don't have data for those datasets; only works for beginning right now.
                    for i in range(2,geneCount):
 
                        masterList[key].append("0")
 
                    masterList[key].append(value)
    
### count length of all arrays in masterList -- if it is not eq to the # of genes that have already been added, then add a 0 at the end
        
        for k, v in masterList.iteritems():
            checkLength = len(v)
            if checkLength < geneCount:
                masterList[k].append("0")
        geneCount = geneCount +  1
    
    for eachDataset in datasetList:
        finalDataList.append(masterList[eachDataset])  
        
    jsonData = json.dumps({'x' : {'Desc' : buttonList },
                           'y' : 
                           { 'vars' : datasetDispList, 
                             'smps' : geneList,
                             'desc': ['Intensity'], 
                             'data' : finalDataList}
                           })
    return jsonData

def filtered_gene_objs(filtersByTypeDict, start, numToReturn):
    where = list()
    filterToCol = {
        "methods" : "datasets.method_id",
        "species" : "datasets.species_id",
        "tissues" : "datasets.uberon_id"
    }

    filterTypes = filtersByTypeDict.keys()
    all_filters = list()

    for key in filtersByTypeDict:
        column = filterToCol[key]
        orQuery = list()

        for each in filtersByTypeDict[key]:
            orQuery.append(column + " = '"+  each + "'")

        one_query = or_(*orQuery)

        where.append(one_query)

    all_filters = and_(*where)
    queriedDs = db.datasets.filter(all_filters).all()
    total = count_by_query(db.datasets.filter(all_filters))

    genes = list()

    count = start
    end = start + numToReturn
 
    for each_dataset in queriedDs:
        peaks = each_dataset.peaks
        while count < end:
            broad_peak = peaks[count-1]
            genes.append(Gene(broad_peak.gene_id))
            count = count + 1

    return genes, total
