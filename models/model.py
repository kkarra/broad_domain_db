import sqlalchemy as sa
from sqlalchemy.ext.sqlsoup import SqlSoup
import json

from config import DB_URI

db = SqlSoup(DB_URI)

# relationships between the tables #

# gene relationships #
db.genes.relate('species', db.species)

db.genes.relate('peaks', db.broad_peaks, 
                    primaryjoin=(db.genes.entrez_gene_id==db.broad_peaks.gene_id))

## checks entrez gene id with mouse id to return human homologs ##
db.genes.relate('human_homologs', db.homology, 
                    primaryjoin=(db.homology.mouse_id==db.genes.entrez_gene_id),
                    foreign_keys=[db.genes.entrez_gene_id])

## checks entrez gene id with human id in homology table to return mouse homologs ##
db.genes.relate('mouse_homologs', db.homology, 
                    primaryjoin=(db.homology.human_id==db.genes.entrez_gene_id),
                    foreign_keys=[db.genes.entrez_gene_id])

## dataset relationships #
db.datasets.relate('method',db.ip_method)
db.datasets.relate('species',db.species)
db.datasets.relate('accession',db.datasets_accession,
                       primaryjoin=(db.datasets.dataset_id==db.datasets_accession.dataset_id))
db.datasets.relate('peaks', db.broad_peaks,
                       primaryjoin=(db.broad_peaks.dataset_id==db.datasets.dataset_id))
db.datasets.relate('tissue', db.anatomy_ontology, 
                       primaryjoin=(db.anatomy_ontology.uberon_id==db.datasets.uberon_id),
                       foreign_keys=[db.anatomy_ontology.uberon_id])
    
## other table and inverse relationships ##
db.datasets_accession.relate('ds', db.datasets)
db.anatomy_ontology.relate('ds', db.datasets, 
                               primaryjoin=(db.anatomy_ontology.uberon_id==db.datasets.uberon_id),
                               foreign_keys=[db.anatomy_ontology.uberon_id])
db.species.relate('ds', db.datasets, 
                      primaryjoin=(db.datasets.species_id==db.species.species_id))
db.broad_peaks.relate('ds', db.datasets)
db.homology.relate('mgene', db.genes, 
                       primaryjoin=(db.homology.mouse_id==db.genes.entrez_gene_id), 
                       foreign_keys=[db.genes.entrez_gene_id])
db.homology.relate('hgene', db.genes,
                       primaryjoin=(db.genes.entrez_gene_id==db.homology.human_id), 
                       foreign_keys=[db.genes.entrez_gene_id])

## joins ##
#joinGeneToPeak = db.join(db.genes, db.broad_peaks, db.genes.entrez_gene_id == db.broad_peaks.gene_id, isouter=True)
    
def all_(type):
    if (type == 'species'):
        return db.species.order_by(db.species.name).all()
    elif (type == 'methods'):
        return db.ip_method.order_by(db.ip_method.wet_name).all()
    elif (type == 'tissues'):
        return db.anatomy_ontology.order_by(db.anatomy_ontology.uberon_description).all()


def count_(type):
    if (type == 'genes'):
        return db.genes.count()
    elif (type == 'species'):
        return db.species.count()
    elif (type == 'methods'):
        return db.methods.count()
    elif (type == 'tissues'):
        return db.anatomy_ontology.count()

def count_by_query(query):
    return query.count()

## returns click event as json object to open gene page for canvasXpress##
