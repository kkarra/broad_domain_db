import os
import json
from flask import Flask, request, jsonify, render_template, \
    redirect, url_for, send_from_directory
from flask.ext.mail import Mail, Message
from werkzeug import secure_filename

from models.model import db, all_, count_
from models.gene import Gene
from models.geneSet import getGeneObjsSlice, multiGeneSignals, \
    filtered_gene_objs, genes_by_genename

UPLOAD_FOLDER = 'temp/uploads'
ALLOWED_EXTENSIONS = set(['txt'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
mail = Mail(app)

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/search')
def search():
    species = all_('species')
    methods = all_('methods')
    tissues = all_('tissues')
    total_genes = count_('genes')
    
    geneList = getGeneObjsSlice(0,10)
    geneListJSON = multiGeneSignals(geneList)
    total_pages = total_genes/10

    return render_template("summary_page.html",
                           gene_list = geneList,
                           max_pages = total_pages,
                           total_results = total_genes,
                           active_pg = 1,
                           methods_list = methods, 
                           tissues_list = tissues, 
                           species_list = species,
                           json_data = geneListJSON,
                           json_config = json.dumps({"graphType" : "Heatmap",
                                                     "gradient" : "true",
                                                     "indicatorWidth" : 3,
                                                     "heatmapType": "red",
                                                     "centerData": "true",
                                                     "autoAdjust": "false",
                                                     "blockFactor": 100
                                                     }),
                           json_event = geneList[0].peak_event
                           )

@app.route('/search/summary/<int:page>')
def summary_pg(page):  
      startRow = (page-1) * 10 
      geneList = getGeneObjsSlice(startRow,10)
      total_genes = count_('genes')
      geneListJSON = multiGeneSignals(geneList)
      total_pages = total_genes/10

      return render_template("summary_page_data.html",
                             gene_list = geneList,
                             max_pages = total_pages,
                             active_pg = page,
                             total_results = total_pages,
                             start = startRow,
                             json_data = geneListJSON,
                             json_config = json.dumps({"graphType" : "Heatmap",
                                                     "gradient" : "true",
                                                     "indicatorWidth" : 3,
                                                     "useFlashIE" : "true",
                                                     "heatmapType": "red",
                                                     "centerData": "true",
                                                       "configuratorWidth": 700}),
                           json_event = geneList[0].peak_event)
 
## to return one gene page
@app.route('/search/<query>')
def show_gene_info(query):
       
    try:
        oneGene = Gene(query)
        
        try:
            oneGene.homolog = Gene(oneGene.homologObj.entrez_gene_id)
        except:
            pass

        return render_template("gene_page.html", gene = oneGene)  
  
 #   except:
  #      try:
   #         gene_list = genes_by_genename(query)
    #        total = len(gene_list)
     #       max_pgs = list_size/10
      #      curr_pg = 1
       #     geneListJSON = multiGeneSignals(gene_list)

        #    return render_template("filter_result.html",
         #                          gene_list = gene_list,
          #                         max_pages = max_pgs,
           #                        total_results = total,
            #                       active_pg = int(curr_pg),
             #                      filter_query = query,
              #                     json_data = geneListJSON,
               #                    json_config = \
                #                       json.dumps({"graphType" : "Heatmap",
                 #                                  "gradient" : "true",
                  #                                 "indicatorWidth" : 3,
                   #                                "useFlashIE" : "true",
                    #                               "heatmapType": "red",
                     #                              "centerData": "true"}),
                      #             json_event = gene_list[0].peak_event
                       #    )
    except:
        return render_template("error/no_gene_err.html",
                               query = query)
  
# filter stuff # 
@app.route('/search/filter/', methods=['POST', 'GET'])
def filter_genes():
    s_query = list()
    t_query = list()
    m_query = list()
    filter_dict = {}

    ## if list of genes then search gene table directly
    genes = request.args.get('genes')
    curr_pg = request.args.get('page') or '1'

    if genes:
        geneList = genes_by_genename(genes)
        total = len(geneList)
        query = genes

    else:
        species = request.args.get('species')
        tissues = request.args.get('tissues')
        methods = request.args.get('methods')

        query = request.args.get('display')
     
        if (species != 'no species filter'):
            s_query = species.split("|")
            filter_dict['species'] = s_query
        if (tissues != 'no tissue filter'):
            t_query = tissues.split("|")
            filter_dict['tissues']= t_query
        if (methods != 'no method filter'):
            m_query = methods.split("|")
            filter_dict['methods'] = m_query
 
        (geneList, total) = filtered_gene_objs(filter_dict, 1, 10)
    
    geneListJSON = multiGeneSignals(geneList)

    maxPgs = total/10
    if (maxPgs < 1):
        maxPgs = 1
    
    return render_template("filter_result.html",
                           gene_list = geneList,
                           max_pages = maxPgs,
                           total_results = total,
                           active_pg = int(curr_pg),
                           filter_query = query,
                           json_data = geneListJSON,
                           json_config = json.dumps({"graphType" : "Heatmap",
                                                     "gradient" : "true",
                                                     "indicatorWidth" : 3,
                                                     "useFlashIE" : "true",
                                                     "heatmapType": "red",
                                                     "centerData": "true"}),
                           json_event = geneList[0].peak_event
                           )
## to take care of uploading files ##
@app.route('/upload', methods=['GET','POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

    return render_template('upload.html')

### to take care of downloading files ##
@app.route('/download', methods=['GET', 'POST'])
def download_file():
    data = json.load(request.args.get('json'))
    text_file = json.load(data)
    
    return text_file

## send email notification that a file was submitted ##
@app.route('/submitted', methods=['POST'])
def send_email():
    text =request.args.get('data')
    msg = Message("File submitted to BDDB",
                  sender="no-reply-bddb@stanford.edu",
                  recipients=['edith.wong@stanford.edu'])

    msg.body = text
    mail.send(msg)

if __name__ == '__main__':
    app.run(debug=True)
